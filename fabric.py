import os
import subprocess 

from waflib import Errors
from waflib.Configure import conf


def options(ctx):
    opt=ctx.add_option_group("Fabric Configuration options")

    fabric_dir = os.environ['FABRIC_DIR'] if os.environ.has_key("FABRIC_DIR") else ''
    
    opt.add_option('--fabric-dir', default=fabric_dir, help='[default %s]' %fabric_dir)


@conf
def find_fabric(ctx):

    searching_path = [ctx.options.fabric_dir]
    for path in os.environ['PATH'].split(os.pathsep):
        searching_path.append(path)
    
    ctx.start_msg('Finding Fabric')

    kl = ctx.find_program('kl', path_list=searching_path, var='FABRIC_KL')
    ctx.find_program('canvas', path_list=searching_path, var='FABRIC_CANVAS')
    ctx.find_program('kl2dfg', path_list=searching_path, var='FABRIC_KL2DFG')

    # kl --version returns status 1 and waf's cmd_and_log fails if return code is not 0.
    # so I will use Popen directly here
    cmd = kl+["--version"]
    p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    (out, err) = p.communicate()
    if err:
        e = Errors.WafError('\nCommand %r failed with error:\n %s' % (cmd, err))
        raise e
    
    version = out.split()[3].split('.')
    ctx.env['FABRIC_VMAJOR'] = version[0]
    ctx.env['FABRIC_VMINOR'] = version[1]

    ctx.env['FABRIC_LOCATION'] = os.path.abspath(os.path.join(kl[0], '../..'))
    ctx.end_msg(ctx.env['FABRIC_LOCATION'])
    

def configure(ctx):
    ctx.find_fabric()




