import sys, os

def check_file(ctx, file_name, exts, paths, start_msg=None, end_msg=None):
    if not start_msg:
        ctx.start_msg('Looking for file {}{}'.format(file_name, exts))
    else:
        ctx.start_msg(start_msg)

    for path in paths:
        for entry in os.listdir(path):
            splitext = os.path.splitext(entry)
            if not os.path.isdir(entry) and splitext[1] in exts and splitext[0]==file_name:
                if not end_msg:
                    ctx.end_msg(os.path.join(path, entry))
                else:
                    ctx.end_msg(end_msg)
                return entry

    ctx.end_msg('NOT FOUND', color='RED')