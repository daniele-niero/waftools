import sys, os
from waflib import Logs
from waflib.Configure import conf

import python

def getMayaDirectory(mayaVersion):
    if sys.platform=="win32":
        return 'C:\\Program Files\\Autodesk\\Maya%s\\bin' %mayaVersion
    elif sys.platform=="darwin":
        return "/Applications/Autodesk/maya%s/Maya.app/Contents/bin" %mayaVersion

def options(ctx):
    opt=ctx.add_option_group("Maya configuration options")

    maya_ver = os.environ.get("MAYA_VERSION", "2019")
    maya_loc = os.environ.get("MAYA_LOCATION", None)
    if maya_loc:
        maya_dir = os.path.join(maya_loc, 'bin')
    else:
        maya_dir = getMayaDirectory(maya_ver)
    
    opt.add_option('--maya-version', default=maya_ver, 
                help='Version of Maya to try to detect. [default %default]')
    opt.add_option('--maya-dir', default=maya_dir,
                help='Autodetected. Override if necessary. [default %default]')

@conf
def find_maya(ctx):
    ctx.start_msg('Finding Maya')

    maya = ctx.find_program('maya', path_list=[ctx.options.maya_dir], var='MAYA')

    maya_v = ctx.cmd_and_log(maya + ['-v']).split(',')[0].split()[1]
    ctx.end_msg(maya[0])
    
    if maya_v != ctx.options.maya_version:
        Logs.warn("Version found (%s) differ from version requested (%s). Version found will be the version taken in account" %(maya_v, ctx.options.maya_version))

        ctx.env['MAYA_VERSION'] = maya_v
    else:
        ctx.env['MAYA_VERSION'] = ctx.options.maya_version

    ctx.msg('', 'Maya Version ' + ctx.env['MAYA_VERSION'])

    ctx.env.append_value('DEFINES', 'MAYA')
    ctx.env['MAYA_LOCATION'] = os.path.abspath(os.path.join(maya[0], '../..'))
    ctx.env.append_value('INCLUDES', os.path.join(ctx.env['MAYA_LOCATION'], 'include'))
    ctx.env.append_value('LIBPATH', os.path.join(ctx.env['MAYA_LOCATION'], 'lib'))
    ctx.msg('Maya Location', ctx.env['MAYA_LOCATION'])


def configure(ctx):
    ctx.find_maya()
    ctx.options.python_name = 'mayapy'
    ctx.options.python_dir = os.path.join(ctx.env['MAYA_LOCATION'], 'bin')

    if sys.platform == "win32":
        if int(ctx.env['MAYA_VERSION']) > 2019:
            python = ctx.find_file(ctx.options.python_name+'.exe', [ctx.options.python_dir])

            program = 'import sys, os;print(os.path.normpath(sys.prefix))'
            py_dir  = os.path.split(ctx.cmd_and_log([python, '-c', program]).rstrip())[1]

            ctx.options.python_include = os.path.join(ctx.env['MAYA_LOCATION'], 'include', py_dir, 'Python')
        else:
            ctx.options.python_include = os.path.join(ctx.env['MAYA_LOCATION'], 'include', 'python2.7')

        ctx.options.python_libpath = os.path.join(ctx.env['MAYA_LOCATION'], 'lib')
    else:
        ctx.options.python_include = None
        ctx.options.python_libpath = None

    ctx.find_python()




