import sys, os
from commons import check_file
from waflib.Configure import conf
from waflib.TaskGen import feature, before_method, after_method


def options(ctx):
    opt=ctx.add_option_group("Python configuration options")

    opt.add_option('--python-dir', default='',
                   help='Override if necessary, for example if you want to use a different python than the default one.')

    opt.add_option('--python-name', default='python',
                   help='Normally changing this option is not necessary, but sometimes it is needed,'
                        ' like with Maya that ships python under the name of mayapy')

    opt.add_option('--python-include', default=None,
                   help='Rarely necessary, this option defines the python\'s include path')

    opt.add_option('--python-libpath', default=None,
                   help='Also rarely necessary, this option defines the python\'s lib path')


@feature('pyext_patterns')
@before_method('propagate_uselib_vars', 'apply_link')
@after_method('apply_bundle')
def init_pyext(self):
    """
    Change the values of *cshlib_PATTERN* and *cxxshlib_PATTERN* to remove the
    *lib* prefix from library names and append the right extension
    """

    if sys.platform=="win32":
        self.env.cxxshlib_PATTERN = self.env.cshlib_PATTERN = '%s.pyd'
    else:
        self.env.cxxshlib_PATTERN = self.env.cshlib_PATTERN = '%s.so'


@conf
def find_python(ctx):
    ctx.start_msg('Finding Python')

    if ctx.options.python_dir:
        path_list = [ctx.options.python_dir]
    else:
        path_list = None
    python = ctx.find_program(ctx.options.python_name, path_list=path_list, var='PYTHON')
    ctx.end_msg(python[0])

    cmd = python + ['-c', 'import sys\nprint(sys.version[:3])']
    py_version = ctx.cmd_and_log(cmd).rstrip()
    ctx.env["PYTHON_VERSION"] = py_version
    ctx.msg("Python Version", py_version)
    if sys.platform == "win32":
        py_version = py_version.replace('.', '')
        lib_prefix = ''
    else:
        lib_prefix = 'lib'

    program = ['import sys, os',
               'from distutils.sysconfig import *',
               'print(os.path.normpath(sys.prefix))',
               'print(get_python_lib(standard_lib=False))',
               'print(get_python_inc())']
    py_dir, py_sitepackages, py_include = ctx.cmd_and_log(python + ['-c', '\n'.join(program)]).splitlines()


    if ctx.options.python_include:
        py_include = ctx.options.python_include

    if ctx.options.python_libpath:
        py_libpath = ctx.options.python_libpath
    elif sys.platform == "win32":
        py_libpath = os.path.join(py_dir, 'libs')
    else:
        py_libpath = os.path.join(py_dir, 'lib')


    ctx.env['PYTHON_DIR'] = py_dir
    ctx.msg("Python Directory", ctx.env['PYTHON_DIR'])

    ctx.env['PYTHON_SITEPACKAGES'] = py_sitepackages
    ctx.msg("Python Site-Packages", ctx.env['PYTHON_SITEPACKAGES'])
    # ctx.env['PREFIX'] = py_sitepackages

    ctx.env['PYTHON_INCLUDE'] = py_include
    ctx.msg("Python Include", ctx.env['PYTHON_INCLUDE'])
    ctx.env['PYTHON_H'] = check_file(ctx, 'Python', ('.h'), (py_include,), start_msg='Looking for Python header')


    ctx.env['PYTHON_LIBPATH'] = py_libpath
    ctx.msg("Python Lib Path", ctx.env['PYTHON_LIBPATH'])
    python_lib = check_file(ctx, lib_prefix+'python'+py_version, ('.a', '.so', '.dylib', '.lib'), (py_libpath,), start_msg='Looking for Python library')
    ctx.env['PYTHON_LIB'] = os.path.splitext(python_lib)[0][len(lib_prefix):]

def configure(ctx):
    ctx.find_python()
